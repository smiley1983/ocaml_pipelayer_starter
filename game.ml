open Td;;

let in_bounds (row, col) =
  row >= 0
  && col >= 0
  && row < Const.height
  && col < Const.width
;;

let connect_offset = [
  ((0, 1), (0, -1));
  ((1, 0), (-1, 0));
  ]
;;

let all_empty grid =
  let result = ref [] in
    Array.iteri (fun ir row -> Array.iteri (fun ic t ->
      if (t = `Empty) then
        result := (ir, ic) :: !result
    ) row) grid;
  !result
;;

let is_valid state player (row, col) =
  List.exists (fun ((par, pac), (pbr, pbc)) ->
    let ar, ac = par + row, pac + col in
    let br, bc = pbr + row, pbc + col in 
    in_bounds (ar, ac) && (in_bounds (br, bc))
    && state.grid.(ar).(ac) = player
    && state.grid.(br).(bc) = player
  ) connect_offset
;;

let valid_moves state player =
  let m = all_empty state.grid in
    List.filter (fun move -> is_valid state player move) m
;;

